#!/usr/bin/python

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

import io
import json
import os
import requests
import sys
import tempfile
import time

import sh

from ansible.module_utils.basic import AnsibleModule


ROLE_PATH = os.environ.get("ANSIBLE_ROLES_PATH", "/opt/ansible/roles")
CONNECT_TIMEOUT = 300
MAX_COMMAND_RETRIES = 10


def run_module():
    module = AnsibleModule(
        argument_spec=dict(
            vault_addr=dict(type="str", required=True),
            vault_token=dict(type="str", required=True),
            oidc_discovery_url=dict(type="str", required=True),
            oidc_client_id=dict(type="str", required=True),
            oidc_client_secret=dict(type="str", required=True),
            policies=dict(type='list', required=False)
        )
    )
    result = {'changed': False}
    env = os.environ
    env['VAULT_ADDR'] = module.params['vault_addr']
    env['VAULT_TOKEN'] = module.params['vault_token']

    for _ in range(CONNECT_TIMEOUT):
        err = io.StringIO()
        try:
            sh.vault.status(_env=env, _err=err)
            break
        except sh.ErrorReturnCode as e:
            time.sleep(1)

    policies = module.params['policies']
    enable_oidc_auth(env)
    create_oidc_config(
        module.params['oidc_discovery_url'],
        module.params['oidc_client_id'],
        module.params['oidc_client_secret'],
        env, 
    )
    create_default_oidc_role(env)
    create_policies(policies, env)
    create_identity_aliases(policies, env)
    module.exit_json(**result)


def enable_oidc_auth(env):
    try:
        sh.vault.auth.enable.oidc(_env=env)
    except sh.ErrorReturnCode as error:
        if 'path is already in use' not in str(error):
            raise Exception(error)
    


def create_oidc_config(url, client, secret, env):
    sh.vault.write(
        'auth/oidc/config',
        f'oidc_discovery_url={url}',
        f'oidc_client_id={client}',
        f'oidc_client_secret={secret}',
        'default_role=default',
        _env=env
    )


def create_default_oidc_role(env):
    addr = f'{env["VAULT_ADDR"]}/ui/vault/auth/oidc/oidc/callback'
    sh.vault.write(
        'auth/oidc/role/default',
        f'allowed_redirect_uris={addr}',
        'user_claim=preferred_username',
        'groups_claim=groups',
        'policies=default',
    )


def create_policies(policies, env):
    for policy in policies:
        sh.vault.policy.write(policy, f'{ROLE_PATH}/vault/files/policy-{policy}.hcl', _env=env)


def _create_group(policy, env):
    out = io.StringIO()
    err = io.StringIO()
    try:
        sh.vault.list(
            '--format=json',
            'identity/group/id',
            _out=out,
            _err=err,
            _env=env
        )
    except sh.ErrorReturnCode as e:
        if e.exit_code != 2:
            raise Exception(e.stderr)

    for id in json.loads(out.getvalue().replace('\n', '').replace('\x1b[0m', '')):
        out = io.StringIO()
        sh.vault.read(
            '--format=json',
            f'identity/group/id/{id}',
            _out=out,
            _env=env
        )
        group = json.loads(out.getvalue().replace('\n', '').replace('\x1b[0m', ''))
        if group['data']['name'] == f'{policy}s':
            return id
    else:
        out = io.StringIO()
        sh.vault.write(
            '-field=id', 
            'identity/group',
            f'name={policy}s',
            f'policies={policy}',
            'type=external',
            _out=out,
            _env=env
        )
        return out.getvalue().replace('\n', '').replace('\x1b[0m', '')


def _get_oidc_accessor(env):
    out = io.StringIO()
    sh.vault.auth.list('-format=json', _env=env, _out=out)
    return json.loads(out.getvalue())['oidc/']['accessor']


def create_identity_aliases(policies, env):
    accessor = _get_oidc_accessor(env)
    for policy in policies:
        group_id = _create_group(policy, env)
        try:
            sh.vault.write(
                'identity/group-alias',
                f'name={policy}s',
                f'mount_accessor={accessor}',
                f'canonical_id={group_id}',
            )
        except sh.ErrorReturnCode as error:
            if 'group alias name is already in use' not in str(error):
                raise Exception(error)
            

def main():
    run_module()


if __name__ == '__main__':
    main()