path "secrets/*" {
    capabilities = ["create", "read", "update", "delete", "list"]
}