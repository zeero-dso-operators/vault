---
- name: add helm repo
  community.kubernetes.helm_repository:
    name: hashicorp
    repo_url: https://helm.releases.hashicorp.com

- import_tasks: admission_controllers.yml

- name: install consul
  community.kubernetes.helm:
    name: consul
    chart_ref: hashicorp/consul
    release_namespace: "{{ ansible_operator_meta.namespace }}"
    values:
      global:
        name: consul

- name: wait until consul pods are running
  community.kubernetes.k8s_info:
    name: consul-server
    api_version: apps/v1
    kind: StatefulSet
    namespace: "{{ ansible_operator_meta.namespace }}"
  register: result
  delay: 3
  retries: 120
  until: 
  - "result.resources[0].status.readyReplicas is defined"
  - "result.resources[0].status.readyReplicas == cluster_size"

- name: create vault ingress
  community.kubernetes.k8s:
    definition:
      apiVersion: extensions/v1beta1
      kind: Ingress
      metadata:
        name: vault
        namespace: "{{ ansible_operator_meta.namespace }}"
        annotations:
          cert-manager.io/cluster-issuer: letsencrypt-prod
          kubernetes.io/ingress.class: nginx
          meta.helm.sh/release-name: keycloak
          meta.helm.sh/release-namespace: keycloak
          nginx.ingress.kubernetes.io/backend-protocol: HTTPS
          nginx.ingress.kubernetes.io/ssl-passthrough: "true"
      spec:
        rules:
        - host: "vault.{{ domain }}"
          http:
            paths:
            - backend:
                serviceName: vault-active
                servicePort: 8200
              path: /
        tls:
        - hosts:
          - "vault.{{ domain }}"
          secretName: tls-cert

- name: wait for vault cert to be ready
  community.kubernetes.k8s_info:
    kind: Certificate
    name: tls-cert
    namespace: "{{ ansible_operator_meta.namespace }}"
  register: tls_cert
  delay: 5
  retries: 100
  until: 
  - "tls_cert.resources is defined"
  - "tls_cert.resources[0].status.conditions[0].status == 'True'"

- name: prepare azure key vault credential
  import_tasks: azure_keyvault.yml
  when: provider == "azure"

- name: install vault
  community.kubernetes.helm:
    name: vault
    chart_ref: hashicorp/vault
    release_namespace: "{{ ansible_operator_meta.namespace }}"
    values: "{{ lookup('template', 'values.yml.j2') | from_yaml }}"

- name: wait until vault pods are running
  community.kubernetes.k8s_info:
    name: vault
    api_version: apps/v1
    kind: StatefulSet
    namespace: "{{ ansible_operator_meta.namespace }}"
  register: result
  delay: 3
  retries: 60
  until:
  - "result.resources[0].status.replicas == cluster_size"

- name: initialize vault
  command: "kubectl exec -it vault-0 -n vault -- vault operator init --tls-skip-verify"
  register: vault_init
  failed_when: 
  - "vault_init.stderr is defined"
  - "'Unable to use a TTY' not in vault_init.stderr"
  - "'Vault is already initialized' not in vault_init.stderr"
  changed_when: vault_init.rc == 0
  delay: 3
  retries: 10
  until: "vault_init.stdout and 'Success! Vault is initialized' in vault_init.stdout"

- name: wait until vault pods are ready
  community.kubernetes.k8s_info:
    name: vault
    api_version: apps/v1
    kind: StatefulSet
    namespace: "{{ ansible_operator_meta.namespace }}"
  register: result
  delay: 3
  retries: 10
  until:
  - "result.resources[0].status.readyReplicas is defined"
  - "result.resources[0].status.readyReplicas == cluster_size"

- set_fact:
    token_regex: "{{ vault_init.stdout | regex_findall('Initial Root Token: (.*)') }}"

- set_fact:
    name: get the vault token
    vault_token: "{{ token_regex[0] }}"
    
- name: create vault root token secret
  community.kubernetes.k8s:
    definition:
      apiVersion: v1
      kind: Secret
      metadata:
        name: vault-token
        namespace: "{{ ansible_operator_meta.namespace }}"
      data:
        root_token: "{{ vault_token | b64encode }}"

- name: create oidc client
  community.kubernetes.k8s:
    definition:
      apiVersion: keycloak.zeero.io/v1
      kind: KeycloakClient
      metadata:
        name: vault
        namespace: "{{ ansible_operator_meta.namespace }}"
      spec:
        domain: "{{ domain }}"
        client_id: vault
        redirect_uri: "https://vault.{{ domain }}/ui/vault/auth/oidc/oidc/callback"

- name: wait for the vault api to be ready
  uri:
    url: https://vault.{{ domain }}/v1/sys/health
  register: response
  until: response.status == 200
  retries: 200
  delay: 3
